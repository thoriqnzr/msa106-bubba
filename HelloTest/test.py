from flask import Flask, request
from flask_restful import Resource, Api

app = Flask(__name__)
api = Api(app)

@app.route("/")
def home():
    return "welcome to the menu!"

class Menu(Resource):
    def get(self):
        return{"Dish" : "Steak"}
              
    

api.add_resource(Menu, "/Menu")

if __name__ == '__main__':
    app.run('0.0.0.0','3333')
