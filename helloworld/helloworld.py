from flask import Flask, request
from flask_restful import Resource, Api
import requests
import pycurl

app = Flask(__name__)
api = Api(app) 

#BASE = "http://localhost:3333/"

response = requests.get("http://localhost:3333/menu")

#response = requests.get('https://github.com/timeline.json')
#response.json()

@app.route("/")
def home():
    return response

#Frontend restful api here will request GET to the 
#backend and fetch data to display here

#task failed module "import requests" unable to work
if __name__ == '__main__':
    app.run('0.0.0.0','3333')
